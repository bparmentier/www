---
title: "About"
date: 2019-08-01T02:34:35+02:00
---

I'm currently working as a Python Back-End developer at [Famoco](https://www.famoco.com) in Brussels, Belgium.

I like Free and Open Source Software, hacker culture, cryptocurrencies,
disassembling (and sometimes reassembling) all kind of things to know how they
work, photography, ellipses, …

I speak Python, Java, C/C++, Bash/Zsh, Regex and sometimes French and
English.

If you are using one of my projects and find it useful, please consider donating
a few [µBTC](bitcoin:168utA5DWMVXLFVfQDahG5abEWUSk9Wcfm "Donate Bitcoin") or
more.

## Contact

You can contact me by email at <bp@brunoparmentier.be>.

My PGP public key fingerprint is `5A3F FFA9 8902 7CEC 9567 9D07 53F6 32B6 E556 BFCA`.
You can find it here: http://keys.gnupg.net/pks/lookup?op=get&search=0x53F632B6E556BFCA

