---
title: "Migrate Nikola Website to Hugo"
date: 2019-11-29T08:28:45+02:00
---

I wanted to migrate my Nikola-based website to Hugo recently.  
Since the front matters are different between the two engines and I had around
50 articles, I wrote some really *quick-and-dirty* scripts to make the
migration easier.

Here is a sample front matter I had in Nikola:

```txt
<!-- 
.. title: Copyleft symbol
.. slug: copyleft-symbol
.. date: 08/13/2013 00:00:00 AM UTC+02:00
.. tags: web, html, css
.. link: 
.. description: 
.. type: text
-->
```

I started with a shell script. A series of sed commands seemed good enough to
get the job done:

```bash
file=$1

sed -i '0,/<!--/s//+++/' $file
sed -i '0,/-->/s//+++/' $file
sed -i '/\.\. type: .*/d' $file
sed -i 's/\.\. title: \(.*\)/title = "\1"/' $file
sed -i 's/\.\. slug: \(.*\)/slug = "\1"/' $file
sed -i 's/\.\. date: \(.*\)/date = \1/' $file
sed -i 's/\.\. tags: \(.*\)/tags = \1/' $file
sed -i 's/\.\. link: \(.*\)/externalLink = "\1"/' $file
sed -i 's/\.\. description: \(.*\)/description = "\1"/' $file
```

Result:

```txt
+++ 
title = "Copyleft symbol"
slug = "copyleft-symbol"
date = 08/13/2013 00:00:00 AM UTC+02:00
tags = web, html, css
externalLink = ""
description = ""
+++
```

But with Hugo, the `tags` element is a list of comma-separated,
double-quoted strings enclosed in two square brackets (aka `["a", "b"]`).  
So I ended up using Python for this:

```python
import re
import sys

def tags_list(match):
    match = match.group(1)
    tags = match.split(', ')
    quoted_tags = [f'"{tag}"' for tag in tags]
    return 'tags = [{}]'.format(', '.join(quoted_tags))

with open(sys.argv[1], 'r') as myfile:
    filedata = myfile.read()

filedata = re.sub(r'tags = (.*)', tags_list, filedata)

with open(sys.argv[1], 'w') as myfile:
    myfile.write(filedata)
```

Executed them with

```bash
for i in $(ls *.md); do
    ./nikola2hugo.sh $i;
    python nikola2hugo.py $i;
done
```

I also had to fix some dates that were wrongly formatted, which I did manually.

Here is the final result:

```txt
+++ 
title = "Copyleft symbol"
slug = "copyleft-symbol"
date = 2013-08-13T00:00:00+02:00
tags = ["web", "html", "css"]
externalLink = ""
description = ""
+++
```
