+++ 
title = "PHP: Use an array value inside a here document"
slug = "php-use-an-array-value-inside-a-here-document"
date = 2014-03-28T00:00:00+02:00
tags = ["web", "php"]
externalLink = ""
description = ""
+++

Just enclose your `$user['name']` with curly brackets.

Example:

```php
<?php
echo <<<END
Hello, my name is {$user['name']} and I live in {$user['country']}.
Today's date is $date.
END;
?>
```
