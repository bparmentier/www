+++ 
title = "Remove the space between two inline-block elements"
slug = "remove-the-space-between-two-inline-block-elements"
date = 2013-01-01T00:00:00+02:00
tags = ["web", "html", "css"]
externalLink = ""
description = ""
+++

*Source: http://css-tricks.com/fighting-the-space-between-inline-block-elements*

Using `display: inline-block` in your CSS file will leave a space between your elements.
Here is a trick to avoid this:

```html
<nav>
    <ul>
        <li><a href="#">Page1</a></li><!--
     --><li><a href="#">Page2</a></li><!--
     --><li><a href="#">Page3</a></li><!--
     --><li><a href="#">Page4</a></li>
    </ul>
</nav>
```

We add a blank comment between `</li>` and `<li>` to "fill" the space.
