+++ 
title = "Configure Vimperator"
slug = "configure-vimperator"
date = 2013-04-17T00:00:00+02:00
tags = ["archlinux", "firefox", "vimperator"]
externalLink = ""
description = ""
+++

Set default search engine:

```
:set defsearch=duckduckgo
```

Set window title:

```
:set titlestring="Mozilla Firefox - Vimperator"
```

You can make these settings default by putting them in `~/.vimperatorrc`.
