+++ 
title = "Firefox: edit cookies from the Developer Toolbar"
slug = "firefox-edit-cookies-from-the-developer-toolbar"
date = 2013-11-17T00:00:00+02:00
tags = ["web", "firefox"]
externalLink = ""
description = ""
+++

Enable the Developer Toolbar with `Alt` + `F2`.

You can set the cookie value with the following command :

```bash
>> cookie set <name> <value> [options]
```
