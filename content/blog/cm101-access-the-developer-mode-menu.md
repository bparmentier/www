+++ 
title = "CM10.1: Access the Developer Mode menu"
slug = "cm101-access-the-developer-mode-menu"
date = 2013-01-01T00:00:00+02:00
tags = ["android", "cyanogenmod"]
externalLink = ""
description = "hello"
+++

Developer Mode is hidden by default in CyanogenMod 10.1.

To activate it, go in `Settings → About phone` and tap six times on Build number.
